# -*- coding: utf-8 -*-

"""
Package: iads
Fichier: kmoyennes.py
Année: semestre 2 - 2018-2019, Sorbonne Université
"""

# ---------------------------
# Fonctions pour les k-moyennes

# Importations nécessaires pour l'ensemble des fonctions de ce fichier:
import pandas as pd
import matplotlib.pyplot as plt

import math
import random

# ---------------------------
# Dans ce qui suit, remplacer la ligne "raise.." par les instructions Python
# demandées.
# ---------------------------

# Normalisation des données :
def normalisation(df):
    """ DataFrame -> DataFrame
        rend le dataframe obtenu par normalisation des données selon 
             la méthode vue en cours 8.
    """
    normalized_df=(df-df.min())/(df.max()-df.min())
    return normalized_df

# -------
# Fonctions distances
def dist_euclidienne_vect(V1,V2):
    dist = (V1-V2)**2
    return dist.sum() ** 0.5

def dist_manhattan_vect(V1,V2):
    dist = abs(V1-V2)
    return dist.sum()

def dist_vect(v1, v2):
    """ Series**2 -> float
        rend la valeur de la distance euclidienne entre les 2 vecteurs
    """
    return dist_euclidienne_vect(v1,v2)
    
def dist_vect(mode,v1, v2):
    """ Series**2 -> float
        rend la valeur de la distance euclidienne entre les 2 vecteurs
    """
    if(mode == "euclidienne"):
        return dist_euclidienne_vect(v1,v2)
    if(mode == "manhattan"):
        return dist_manhattan_vect(v1,v2)
    
# -------
# Calculs de centroïdes :
# ************************* Recopier ici la fonction centroide()
def centroide(df):
    """ DataFrame -> DataFrame
        Hypothèse: len(M) > 0
        rend le centroïde des exemples contenus dans M
    """
    return df.mean().to_frame().T

# -------
# Inertie des clusters :
# ************************* Recopier ici la fonction inertie_cluster()
def inertie_cluster(df):
    """ DataFrame -> float
        DF: DataFrame qui représente un cluster
        L'inertie est la somme (au carré) des distances des points au centroide.
    """
    cf = centroide(df)
    shape = df.shape
    inertie = 0
    for i in range(shape[0]):
        inertie += dist_vect("euclidienne",df.iloc[i],cf.iloc[0])**2
    return inertie

# -------
# Algorithmes des K-means :
def tirage(VX, m, remise):
    if(len(VX) < m):
        m = len(VX)
    if(remise):
        tirage = []
        for i in range (m):
            choix = random.choice(VX)
            #print(choix)
            tirage.append(random.choice(VX))
    else:
        tirage = random.sample(VX,m)
    return tirage

def removeList(liste,listeR):
    retour = liste
    for i in listeR:
        retour.remove(i)
    return retour

def initialisation(n,df):
    """ int * DataFrame -> DataFrame
        K : entier >1 et <=n (le nombre d'exemples de DF)
        DF: DataFrame contenant n exemples
    """
    shape = df.shape
    listeElement = list(range(shape[0]))
    nbTirage = math.ceil(shape[0]/n)
    listeDF = []
    for i in range(n):
        listeEnsemble = tirage(listeElement,1,False)
        listeElement = removeList(listeElement,listeEnsemble)
        newDF = df.iloc[listeEnsemble[0]].to_frame().T
        listeDF.append(newDF)
    result = pd.concat(listeDF)
    return result

# -------
def plus_proche(exemple,df):
    """ Series * DataFrame -> int
        Exe : Series contenant un exemple
        Centres : DataFrame contenant les K centres
    """
    size = df.shape[0]
    dist_min = math.inf
    indice_min = -1
    for i in range(size):
        dist = dist_vect("euclidienne",df.iloc[i],exemple)
        if(dist < dist_min):
            indice_min = i
            dist_min = dist
     
    return indice_min
# -------
def affecte_cluster(df,ensemble):
    """ DataFrame * DataFrame -> dict[int,list[int]]
        Base: DataFrame contenant la base d'apprentissage
        Centres : DataFrame contenant des centroides
    """
    dictionnaire = dict()
    for i in range(ensemble.shape[0]):
        dictionnaire[i] = []
    
    for i in range(df.shape[0]):
        indice = plus_proche(df.iloc[i],ensemble)
        

        dictionnaire[indice].append(i)
    
    return dictionnaire
# -------
def nouveaux_centroides(df,mAffect):
    """ DataFrame * dict[int,list[int]] -> DataFrame
        Base : DataFrame contenant la base d'apprentissage
        U : Dictionnaire d'affectation
    """
    listeDF = []
    for l in mAffect.values():
        liste = []
        for i in l:
            liste.append(i)
        newDF = centroide(df.loc[liste])
        listeDF.append(newDF)
    result = pd.concat(listeDF)
    return result
# -------
def inertie_globale(df, mAffect):
    """ DataFrame * dict[int,list[int]] -> float
        Base : DataFrame pour la base d'apprentissage
        U : Dictionnaire d'affectation
    """
    inertieGlobale = 0
    for l in mAffect.values():    
        liste = []
        for i in l:
            liste.append(i)
        newDF = df.loc[liste]
        inertieCluster = inertie_cluster(newDF)
        inertieGlobale += inertieCluster
    return inertieGlobale
        
# -------
def kmoyennes(k,df,epsilon,iter_max):
    """ int * DataFrame * float * int -> tuple(DataFrame, dict[int,list[int]])
        K : entier > 1 (nombre de clusters)
        Base : DataFrame pour la base d'apprentissage
        epsilon : réel >0
        iter_max : entier >1
    """
    centroides = initialisation(k,df)
    mAffect = affecte_cluster(df,centroides)
    inertie = math.inf
    newInertie = inertie_globale(df,mAffect)
    for i in range(iter_max):
        difference = abs(newInertie - inertie) 
        if(inertie == math.inf):
            difference = inertie
        if(difference < epsilon and difference != math.inf):
            break;
        centroides = nouveaux_centroides(df,mAffect)
        mAffect = affecte_cluster(df,centroides)
        inertie = newInertie
        newInertie = inertie_globale(df,mAffect)
        print("Itération ",i," mAffect : ",mAffect)
    return centroides,mAffect
# -------
# Affichage :
# ************************* Recopier ici la fonction affiche_resultat()
def affiche_resultat(df,les_centres,mAffect):
    """ DataFrame **2 * dict[int,list[int]] -> None
    """    
    # Remarque: pour les couleurs d'affichage des points, quelques exemples:
    # couleurs =['darkviolet', 'darkgreen', 'orange', 'deeppink', 'slateblue', 'orangered','y', 'g', 'b']
    # voir aussi (google): noms des couleurs dans matplolib
    listeColor =['darkviolet', 'darkgreen', 'orange', 'deeppink', 'slateblue', 'orangered','y', 'g', 'b']
    cpt = 0
    for l in mAffect.values():    
        liste = []
        for i in l:
            liste.append(i)
        newDF = df.loc[liste]
        plt.scatter(newDF['X'],newDF['Y'],color=listeColor[cpt])
        cpt += 1
    plt.scatter(les_centres['X'],les_centres['Y'],color='r',marker='x')
# -------