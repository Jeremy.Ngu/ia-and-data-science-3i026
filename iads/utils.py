# -*- coding: utf-8 -*-

"""
Package: iads
Fichier: utils.py
Année: semestre 2 - 2018-2019, Sorbonne Université
"""

# ---------------------------
# Fonctions utiles pour les TDTME de 3i026

# import externe
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# importation de LabeledSet
from . import LabeledSet as ls

def plot2DSet(set):
    """ LabeledSet -> NoneType
        Hypothèse: set est de dimension 2
        affiche une représentation graphique du LabeledSet
        remarque: l'ordre des labels dans set peut être quelconque
    """
    S_pos = set.x[np.where(set.y == 1),:][0]      # tous les exemples de label +1
    S_neg = set.x[np.where(set.y == -1),:][0]     # tous les exemples de label -1
    plt.scatter(S_pos[:,0],S_pos[:,1],marker='o') # 'o' pour la classe +1
    plt.scatter(S_neg[:,0],S_neg[:,1],marker='x') # 'x' pour la classe -1

def plot_frontiere(set,classifier,step=100):
    """ LabeledSet * Classifier * int -> NoneType
        Remarque: le 3e argument est optionnel et donne la "résolution" du tracé
        affiche la frontière de décision associée au classifieur
    """
    mmax=set.x.max(0)
    mmin=set.x.min(0)
    x1grid,x2grid=np.meshgrid(np.linspace(mmin[0],mmax[0],step),np.linspace(mmin[1],mmax[1],step))
    grid=np.hstack((x1grid.reshape(x1grid.size,1),x2grid.reshape(x2grid.size,1)))
    
    # calcul de la prediction pour chaque point de la grille
    res=np.array([classifier.predict(grid[i,:]) for i in range(len(grid)) ])
    res=res.reshape(x1grid.shape)
    # tracer des frontieres
    plt.contourf(x1grid,x2grid,res,colors=["red","cyan"],levels=[-1000,0,1000])
    
# ------------------------ 

def createGaussianDataset(positive_center, positive_sigma, negative_center, negative_sigma, nb_points):
    """ 
        rend un LabeledSet 2D généré aléatoirement.
        Arguments:
        - positive_center (vecteur taille 2): centre de la gaussienne des points positifs
        - positive_sigma (matrice 2*2): variance de la gaussienne des points positifs
        - negative_center (vecteur taille 2): centre de la gaussienne des points négative
        - negative_sigma (matrice 2*2): variance de la gaussienne des points négative
        - nb_points (int):  nombre de points de chaque classe à générer
    """
    LS = ls.LabeledSet(2)
    
    positive_test = np.random.multivariate_normal(positive_center,positive_sigma,nb_points)
    negative_test = np.random.multivariate_normal(negative_center,negative_sigma,nb_points)
    for i in range (nb_points):
        LS.addExample(positive_test[i],1)
        LS.addExample(negative_test[i],-1)
    return LS
    
    
import math 

def concatenate(ls1,ls2):
    LS = ls.LabeledSet(ls1.getInputDimension())
    
    for i in range(ls1.size()):
        LS.addExample(ls1.getX(i),ls1.getY(i))
    for i in range(ls2.size()):
        LS.addExample(ls2.getX(i),ls2.getY(i))
    return LS
    
def createXOR(nb_points,var):
    sigma = var
    
    ls1 = createGaussianDataset(np.array([0,0]),sigma * np.array([[1,0],[0,1]]),np.array([1,0]),sigma * np.array([[1,0],[0,1]]),math.ceil(nb_points/2))
    ls2 = createGaussianDataset(np.array([1,1]),sigma * np.array([[1,0],[0,1]]),np.array([0,1]),sigma * np.array([[1,0],[0,1]]),math.floor(nb_points/2))
    
    
    
    return concatenate(ls1,ls2)

def shuffle_ls(labeledSet):
    
    size = labeledSet.size()
    input_dimension = labeledSet.getInputDimension()
    
    new_LS = ls.LabeledSet(input_dimension)
    
    if size == 0:
        return
    
    npx = np.empty((size,input_dimension), np.double)
    npy = np.empty((size,1),np.double)
    
    for i in range(0,labeledSet.size()):
        npx[i] = labeledSet.getX(i)
        npy[i] = labeledSet.getY(i)
    
    
    matrix_res = np.concatenate((npx,npy),axis=1)
    
    np.random.shuffle(matrix_res)
    #print(matrix_res)

    res = np.array_split(matrix_res,input_dimension,axis=1)
    
    for i in range(size):
        new_LS.addExample(res[0][i],res[1][i])
    return new_LS


def split_set(labeledSet,PourcentageTraining):
    input_dimension = labeledSet.getInputDimension()
    
    lstn = ls.LabeledSet(input_dimension)
    lstt = ls.LabeledSet(input_dimension)
    
    limitTN = labeledSet.size() * PourcentageTraining
    cpt = 0;
    
    
    for i in range (labeledSet.size()):
        if (i < limitTN ):
            lstn.addExample(labeledSet.getX(i),labeledSet.getY(i))
            cpt += 1
        else:
            lstt.addExample(labeledSet.getX(i),labeledSet.getY(i))
    return lstn,lstt



def shuffle_ls2(labeledSet):
    
    size = labeledSet.size()
    input_dimension = labeledSet.getInputDimension()
    
    new_LS = ls.LabeledSet(input_dimension)
    
    if size == 0:
        return
    
    npx = np.empty((size,input_dimension), np.double)
    npy = np.empty((size,1),np.double)
    
    for i in range(0,labeledSet.size()):
        npx[i] = labeledSet.getX(i)
        npy[i] = labeledSet.getY(i)
    
    
    matrix_res = np.concatenate((npx,npy),axis=1)
    
    np.random.shuffle(matrix_res)
    #print(matrix_res)
    
    res = np.array_split(matrix_res,np.array((0,input_dimension)),axis=1)
    
    #print(res[1],res[2])
    for i in range(size):
        new_LS.addExample(res[1][i],res[2][i])
    return new_LS