import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
%matplotlib inline  

# La ligne suivante permet de préciser le chemin d'accès à la librairie iads
import sys
sys.path.append('D:/Programmation/3I026')

# Importation de la librairie iads
import iads as iads

# importation de LabeledSet
from iads import LabeledSet as ls

# importation de Classifiers
from iads import Classifiers as cl

# importation de utils
from iads import utils as ut

class KernelBias:
    def transform(self,x):
        y=np.asarray([x[0],x[1],1])
        return y

class KernelPoly:
    def transform(self,x):
        x1 = x[0]
        x2 = x[1]
        y=np.asarray([1,x1,x2,x1*x1,x2*x2,x1*x2])
        return y
    
class ClassifierPerceptronKernel(cl.Classifier):
    def __init__(self,dimension_kernel,learning_rate,kernel):
        """ Argument:
                - intput_dimension (int) : dimension d'entrée des exemples
                - learning_rate :
            Hypothèse : input_dimension > 0
        """
        ##TODO
        self.k = kernel
        self.lr = learning_rate
        self.input_dimension = dimension_kernel

        v = np.random.rand(self.input_dimension)     # vecteur aléatoire à input_dimension dimensions
        self.w = (2* v - 1) / np.linalg.norm(v) # on normalise par la norme de v
        #print(self.w)
        
        
    def predict(self,x):
        """ rend la prediction sur x (-1 ou +1)
        """
        z = np.dot(self.k.transform(x), self.w)
        if(z >= 0):
            return 1
        if(z < 0):
            return -1
        
    def train(self,labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """
        
        #print("Before training",self.w)
        
        for k in range(labeledSet.size()):
            lr = self.lr
            x = self.k.transform(labeledSet.getX(k))
            y = labeledSet.getY(k)
            yt = self.predict(labeledSet.getX(k))
            #print(self.w,y,x)

            #print("Accuracy : ",self.accuracy(labeledSet))
            
            self.w = self.w + lr*(y-yt)*x
            
            #print("lr*y*x : ",lr*(y*x))
        #print("After Training",self.w)    