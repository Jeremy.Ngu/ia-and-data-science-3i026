# -*- coding: utf-8 -*-

"""
Package: iads
Fichier: Classifiers.py
Année: semestre 2 - 2018-2019, Sorbonne Université
"""

# Import de packages externes
import numpy as np
import pandas as pd

# ---------------------------
class Classifier:
    """ Classe pour représenter un classifieur
        Attention: cette classe est une classe abstraite, elle ne peut pas être
        instanciée.
    """
 
     #TODO: A Compléter

    def __init__(self, input_dimension):
        """ Constructeur de Classifier
            Argument:
                - intput_dimension (int) : dimension d'entrée des exemples
            Hypothèse : input_dimension > 0
        """
        raise NotImplementedError("Please Implement this method")
        
    def predict(self, x):
        """ rend la prediction sur x (-1 ou +1)
        """
        raise NotImplementedError("Please Implement this method")

    def train(self, labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """
        
        raise NotImplementedError("Please Implement this method")
    
    def accuracy(self, dataset):
        """ Permet de calculer la qualité du système 
        """
        
        size = dataset.size()
        good_result = 0
        
        for i in range (size):
            prediction = self.predict(dataset.getX(i))
            result = dataset.getY(i)
            #print("Prediction : ",prediction," // Result : ",result)
            if ( prediction == result ):
                good_result += 1
            
        
        probability_good_result = good_result / size 
        pourcentage_good_result = (good_result * 100) / size
        
        return pourcentage_good_result

# ---------------------------
class ClassifierLineaireRandom(Classifier):
    """ Classe pour représenter un classifieur linéaire aléatoire
        Cette classe hérite de la classe Classifier
    """
    
    #TODO: A Compléter
    
    def __init__(self, input_dimension):
        """ Constructeur de Classifier
            Argument:
                - intput_dimension (int) : dimension d'entrée des exemples
            Hypothèse : input_dimension > 0
        """
        raise NotImplementedError("Please Implement this method")
        
    def predict(self, x):
        """ rend la prediction sur x (-1 ou +1)
        """
        raise NotImplementedError("Please Implement this method")

    def train(self, labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """        
        raise NotImplementedError("Please Implement this method")
    
# ---------------------------
class ClassifierKNN(Classifier):
    
    def __init__(self,input_dimension,neighbors):
        self.k = neighbors
        self.input_dimension = 2
        
    def train(self,labeledSet):
        self.ls = labeledSet
        
        
    def predict(self,x):
        size = self.ls.size()
        dist_table = np.array([-1,-1])
        
        for i in range (size):
            lsX = self.ls.getX(i)
            dist = 0
            
            for j in range(self.input_dimension):
                dist += (lsX[j] - x[j])**2
            
            
            to_add = np.array([i,dist])
            dist_table = np.vstack((dist_table,to_add))
        
        #dist_table = np.argsort(dist_table,axis=0)
        dist_table = dist_table[dist_table[:, 1].argsort()]
        
        #print("\n",dist_table)
        
        resultat = 0
        for i in range (self.k):
            indice = int(dist_table[i+1][0])
            lsY = self.ls.getY(indice)
            resultat += lsY
            
            #print(indice,lsY)
            
        if(resultat >= 0):
            return 1
        else:
            return -1
           

# ---------------------------
