import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import math 
import os 
from datetime import datetime as dt
import random

# La ligne suivante permet de préciser le chemin d'accès à la librairie iads
import sys
sys.path.append('D:/Programmation/3I026')
sys.path.append('D:/Programmation/3I026/ml_lastest_small')

# Importation de la librairie iads
import iads as iads

# importation de LabeledSet
from iads import LabeledSet as ls

# importation de Classifiers
from iads import Classifiers as cl

# importation de utils
from iads import utils as ut


#### TME 03 Perceptron
###
## 
class ClassifierPerceptronRandom(cl.Classifier):
    def __init__(self, input_dimension):
        """ Argument:
                - input_dimension (int) : dimension d'entrée des exemples
            Hypothèse : input_dimension > 0
        """
        v = np.random.rand(input_dimension)     # vecteur aléatoire à input_dimension dimensions
        self.w = (2* v - 1) / np.linalg.norm(v) # on normalise par la norme de v

    def predict(self, x):
        """ rend la prediction sur x (-1 ou +1)
        """
        z = np.dot(x, self.w)
        if(z >= 0):
            return 1
        if(z < 0):
            return -1
        
    def train(self,labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """        
        print("No training needed")
        

class ClassifierPerceptron(cl.Classifier):
    """ Perceptron de Rosenblatt
    """
    def __init__(self,input_dimension,learning_rate):
        """ Argument:
                - intput_dimension (int) : dimension d'entrée des exemples
                - learning_rate :
            Hypothèse : input_dimension > 0
        """
        
        v = np.random.rand(input_dimension)     # vecteur aléatoire à input_dimension dimensions
        self.w = (2* v - 1) / np.linalg.norm(v) # on normalise par la norme de v
        self.lr = learning_rate
        self.input_dimension = input_dimension

    def predict(self,x):
        """ rend la prediction sur x (-1 ou +1)
        """
        z = np.dot(x, self.w)
        if(z >= 0):
            return 1
        if(z < 0):
            return -1

    
    def train(self,labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """
        
        #print("Before training W = ",self.w)
        
        for k in range(labeledSet.size()):
            lr = self.lr
            x = labeledSet.getX(k)
            y = labeledSet.getY(k)



            #print("Accuracy : ",self.accuracy(labeledSet))
                
            self.w = self.w + lr*(y*x)
        
        #print("After Training W = ",self.w)
        
def split_set(labeledSet,PourcentageTraining):
    input_dimension = labeledSet.getInputDimension()
    
    lstn = ls.LabeledSet(input_dimension)
    lstt = ls.LabeledSet(input_dimension)
    
    limitTN = labeledSet.size() * PourcentageTraining/100
    cpt = 0;
    
    
    for i in range (labeledSet.size()):
        if (i < limitTN ):
            lstn.addExample(labeledSet.getX(i),labeledSet.getY(i))
            cpt += 1
        else:
            lstt.addExample(labeledSet.getX(i),labeledSet.getY(i))
    return lstn,lstt



def concatenate(ls1,ls2):
    LS = ls.LabeledSet(ls1.getInputDimension())
    
    for i in range(ls1.size()):
        LS.addExample(ls1.getX(i),ls1.getY(i))
    for i in range(ls2.size()):
        LS.addExample(ls2.getX(i),ls2.getY(i))
    return LS
    
def createXOR(nb_points,var):
    sigma = var
    
    ls1 = ut.createGaussianDataset(np.array([0,0]),sigma * np.array([[1,0],[0,1]]),np.array([1,0]),sigma * np.array([[1,0],[0,1]]),math.ceil(nb_points/2))
    ls2 = ut.createGaussianDataset(np.array([1,1]),sigma * np.array([[1,0],[0,1]]),np.array([0,1]),sigma * np.array([[1,0],[0,1]]),math.floor(nb_points/2))
    
    
    
    return concatenate(ls1,ls2)

def shuffle_ls(labeledSet):
    
    size = labeledSet.size()
    input_dimension = labeledSet.getInputDimension()
    
    new_LS = ls.LabeledSet(input_dimension)
    
    if size == 0:
        return
    
    npx = np.empty((size,input_dimension), np.double)
    npy = np.empty((size,1),np.double)
    
    for i in range(0,labeledSet.size()):
        npx[i] = labeledSet.getX(i)
        npy[i] = labeledSet.getY(i)
    
    
    matrix_res = np.concatenate((npx,npy),axis=1)
    
    np.random.shuffle(matrix_res)
    #print(matrix_res)
    
    res = np.array_split(matrix_res,np.array((0,input_dimension)),axis=1)
    
    #print(res[1],res[2])
    for i in range(size):
        new_LS.addExample(res[1][i],res[2][i])
    return new_LS



class KernelBias:
    def transform(self,x):
        y=np.asarray([x[0],x[1],1])
        return y
    
class ClassifierPerceptronKernel(cl.Classifier):
    def __init__(self,dimension_kernel,learning_rate,kernel):
        """ Argument:
                - intput_dimension (int) : dimension d'entrée des exemples
                - learning_rate :
            Hypothèse : input_dimension > 0
        """
        ##TODO
        self.k = kernel
        self.lr = learning_rate
        self.input_dimension = dimension_kernel

        v = np.random.rand(self.input_dimension)     # vecteur aléatoire à input_dimension dimensions
        self.w = (2* v - 1) / np.linalg.norm(v) # on normalise par la norme de v
        #print(self.w)
        
        
    def predict(self,x):
        """ rend la prediction sur x (-1 ou +1)
        """
        z = np.dot(self.k.transform(x), self.w)
        if(z >= 0):
            return 1
        if(z < 0):
            return -1
        
    def train(self,labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """
        
        #print("Before training",self.w)
        
        for k in range(labeledSet.size()):
            lr = self.lr
            x = self.k.transform(labeledSet.getX(k))
            y = labeledSet.getY(k)
            yt = self.predict(labeledSet.getX(k))
            #print(self.w,y,x)

            #print("Accuracy : ",self.accuracy(labeledSet))
            
            self.w = self.w + lr*(y-yt)*x
            
            #print("lr*y*x : ",lr*(y*x))
        #print("After Training",self.w)
        
        
class KernelPoly:
    def transform(self,x):
        x1 = x[0]
        x2 = x[1]
        y=np.asarray([1,x1,x2,x1*x1,x2*x2,x1*x2])
        return y
    
class Kernel11:
    def transform(self,x):
        print(x,[1])
        y = np.concatenate((x,np.array([1])),axis=0)
        return y

k = Kernel11()

def shuffle_ls2(labeledSet):
    
    size = labeledSet.size()
    input_dimension = labeledSet.getInputDimension()
    
    new_LS = ls.LabeledSet(input_dimension)
    
    if size == 0:
        return
    
    npx = np.empty((size,input_dimension), np.double)
    npy = np.empty((size,1),np.double)
    
    for i in range(0,labeledSet.size()):
        npx[i] = labeledSet.getX(i)
        npy[i] = labeledSet.getY(i)
    
    
    matrix_res = np.concatenate((npx,npy),axis=1)
    
    np.random.shuffle(matrix_res)
    #print(matrix_res)
    
    res = np.array_split(matrix_res,np.array((0,input_dimension)),axis=1)
    
    #print(res[1],res[2])
    for i in range(size):
        new_LS.addExample(res[1][i],res[2][i])
    return new_LS


#### TME 04
###
## 
class ClassifierPerceptronStoch(cl.Classifier):
    def __init__(self, input_dimension,lr):
        """ Argument:
                - input_dimension (int) : dimension d'entrée des exemples
            Hypothèse : input_dimension > 0
        """
        v = np.random.rand(input_dimension)     # vecteur aléatoire à input_dimension dimensions
        self.w = (2* v - 1) / np.linalg.norm(v) # on normalise par la norme de v
        self.lr = lr
    def predict(self, x):
        """ rend la prediction sur x (-1 ou +1)
        """
        z = np.dot(x, self.w)
        if(z >= 0):
            return 1
        if(z < 0):
            return -1
        
    def train(self,labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """        
        m = labeledSet.size()
        for k in range(m):
            
            lr = self.lr
            x = labeledSet.getX(k)
            y = labeledSet.getY(k)

            #print("Accuracy : ",self.accuracy(labeledSet))
            
            self.w = self.w + lr*(y-(self.w*x))*x
            


#### TME 03 Perceptron 
###
## 
class ClassifierPerceptronStoch(cl.Classifier):
    def __init__(self, input_dimension,lr):
        """ Argument:
                - input_dimension (int) : dimension d'entrée des exemples
            Hypothèse : input_dimension > 0
        """
        v = np.random.rand(input_dimension)     # vecteur aléatoire à input_dimension dimensions
        self.w = (2* v - 1) / np.linalg.norm(v) # on normalise par la norme de v
        self.lr = lr
    def predict(self, x):
        """ rend la prediction sur x (-1 ou +1)
        """
        z = np.dot(x, self.w)
        if(z >= 0):
            return 1
        if(z < 0):
            return -1
        
    def train(self,labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """        
        m = labeledSet.size()
        for k in range(m):
            
            lr = self.lr
            x = labeledSet.getX(k)
            y = labeledSet.getY(k)

            #print("Accuracy : ",self.accuracy(labeledSet))
            
            self.w = self.w + lr*(y-(self.w*x))*x
            
            
class ClassifierPerceptronBatch(cl.Classifier):
    def __init__(self, input_dimension,lr):
        """ Argument:
                - input_dimension (int) : dimension d'entrée des exemples
            Hypothèse : input_dimension > 0
        """
        v = np.random.rand(input_dimension)     # vecteur aléatoire à input_dimension dimensions
        self.w = (2* v - 1) / np.linalg.norm(v) # on normalise par la norme de v
        self.lr = lr
    def predict(self, x):
        """ rend la prediction sur x (-1 ou +1)
        """
        z = np.dot(x, self.w)
        if(z >= 0):
            return 1
        if(z < 0):
            return -1
        
    def train(self,labeledSet):
        """ Permet d'entrainer le modele sur l'ensemble donné
        """        
        m = labeledSet.size()
        g = 0
        lr = self.lr/m
        for k in range(m):
            

            x = labeledSet.getX(k)
            y = labeledSet.getY(k)

            
            #print("Accuracy : ",self.accuracy(labeledSet))
            #Vote de chaque valeur
            g += (y-(self.w*x))*x
            
        self.w = self.w + lr * g
        
#### TME 05 Arbre Décisionnel
###
## 
def classe_majoritaire(labeledSet):
    array = np.empty((0,0))
    size = labeledSet.size()
    
    # Permet de récupérer un dictionnaire où la clé est le retour et la valeur le nombre d'occurences
    for i in range(size):
        array = np.append(array,labeledSet.getY(i))
    unique, counts = np.unique(array, return_counts=True)
    
    classe = 1 # Par defaut on renvoie la classe 1 si aucune n'est majoritaire à plus de 50%
    cpt = 0
    
    for i in range (unique.size):
        if (cpt < counts[i]):
            classe = unique[i]
    return classe
    
def shannon(P):
    somme = 0
    iteration = len(P)
    if(iteration == 0):
        return 0
    for i in range (iteration):
        #print(P[i])
        if(P[i] == 1):
            return 0
        if(P[i] != 0):
            #print(P[i],iteration)
            somme += P[i] * math.log(P[i],iteration)
    return -somme

def entropie(labeledSet):
    # Creation d'une array pour stocker les différentes valeurs de retour possible
    array = np.empty((0,0))
    size = labeledSet.size()
    
    # Permet de récupérer un dictionnaire où la clé est le retour et la valeur le nombre d'occurences
    for i in range(size):
        array = np.append(array,labeledSet.getY(i))
    unique, counts = np.unique(array, return_counts=True)
    
    # Probabilité = Nombre d'occurences de chaque parametre / la taille de l'ensemble
    liste_prob = counts/size
    return shannon(liste_prob)
    
def discretise(LSet, col):
    """ LabelledSet * int -> tuple[float, float]
        Hypothèse: LSet.size() >= 2
        col est le numéro de colonne sur X à discrétiser
        rend la valeur de coupure qui minimise l'entropie ainsi que son entropie.
    """
    # initialisation:
    min_entropie = 1.1  # on met à une valeur max car on veut minimiser
    min_seuil = 0.0     
    # trie des valeurs:
    #print("LSet.x : ",LSet.x.shape)
    ind= np.argsort(LSet.x,axis=0)
    #print("ind shape : ",ind.shape)
    #print("lset size : ",LSet.size())
    
    # calcul des distributions des classes pour E1 et E2:
    inf_plus  = 0               # nombre de +1 dans E1
    inf_moins = 0               # nombre de -1 dans E1
    sup_plus  = 0               # nombre de +1 dans E2
    sup_moins = 0               # nombre de -1 dans E2       
    # remarque: au départ on considère que E1 est vide et donc E2 correspond à E. 
    # Ainsi inf_plus et inf_moins valent 0. Il reste à calculer sup_plus et sup_moins 
    # dans E.
    for j in range(0,LSet.size()):
        if (LSet.getY(j) == -1):
            sup_moins += 1
        else:
            sup_plus += 1
    nb_total = (sup_plus + sup_moins) # nombre d'exemples total dans E
    
    # parcours pour trouver le meilleur seuil:
    for i in range(len(LSet.x)-1):
        v_ind_i = ind[i]   # vecteur d'indices
        """
        print("Itération ",i)
        print("Vecteur d'indice :",v_ind_i)
        print("col : ",col)
        print("v_ind_i[col] :",(v_ind_i[col]))
        print("Discrétise",LSet.getX(v_ind_i[col]))
        """
        courant = LSet.getX(v_ind_i[col])[col]
        lookahead = LSet.getX(ind[i+1][col])[col]
        val_seuil = (courant + lookahead) / 2.0;
        # M-A-J de la distrib. des classes:
        # pour réduire les traitements: on retire un exemple de E2 et on le place
        # dans E1, c'est ainsi que l'on déplace donc le seuil de coupure.
        if LSet.getY(ind[i][col])[0] == -1:
            inf_moins += 1
            sup_moins -= 1
        else:
            inf_plus += 1
            sup_plus -= 1
        # calcul de la distribution des classes de chaque côté du seuil:
        nb_inf = (inf_moins + inf_plus)*1.0     # rem: on en fait un float pour éviter
        nb_sup = (sup_moins + sup_plus)*1.0     # que ce soit une division entière.
        # calcul de l'entropie de la coupure
        val_entropie_inf = shannon([inf_moins / nb_inf, inf_plus  / nb_inf])
        val_entropie_sup = shannon([sup_moins / nb_sup, sup_plus  / nb_sup])
        val_entropie = (nb_inf / nb_total) * val_entropie_inf \
                       + (nb_sup / nb_total) * val_entropie_sup
        # si cette coupure minimise l'entropie, on mémorise ce seuil et son entropie:
        if (min_entropie > val_entropie):
            min_entropie = val_entropie
            min_seuil = val_seuil
    return (min_seuil, min_entropie)

def divise(LSet,att,seuil):
    """ LSet: LabeledSet
        att : numéro d'attribut
        seuil : valeur de seuil
        rend le tuple contenant les 2 sous-LabeledSet obtenus par la
        division de LSet selon le seuil sur l'attribut att
    """
    input_dimenssion = LSet.getInputDimension()
    
    # Si la dimension a examiner est plus grande ou égale aux nombre de dimension du labeledset
    # Alors on ne renvoie rien. Probleme de paramètre
    if att >= input_dimenssion:
        return 
    
    
    # LabeledSet de retour
    ls1 = ls.LabeledSet(input_dimenssion)
    ls2 = ls.LabeledSet(input_dimenssion)
    
    
    size = LSet.size()
    
    # Split du LabeledSet selon le critère seuil
    for i in range(size):
        X = LSet.getX(i)
        Y = LSet.getY(i)

        if (X[att] <= seuil):
            ls1.addExample(X,Y)
        else:
            ls2.addExample(X,Y)
    
    return ls1,ls2


class ArbreBinaire:
    def __init__(self):
        self.attribut = None   # numéro de l'attribut
        self.seuil = None
        self.inferieur = None # ArbreBinaire Gauche (valeurs <= au seuil)
        self.superieur = None # ArbreBinaire Gauche (valeurs > au seuil)
        self.classe = None # Classe si c'est une feuille: -1 ou +1
        
    def est_feuille(self):
        """ rend True si l'arbre est une feuille """
        return self.seuil == None
    
    def ajoute_fils(self,ABinf,ABsup,att,seuil):
        """ ABinf, ABsup: 2 arbres binaires
            att: numéro d'attribut
            seuil: valeur de seuil
        """
        self.attribut = att
        self.seuil = seuil
        self.inferieur = ABinf
        self.superieur = ABsup
    
    def ajoute_feuille(self,classe):
        """ classe: -1 ou + 1
        """
        self.classe = classe
        
    def classifie(self,exemple):
        """ exemple : numpy.array
            rend la classe de l'exemple: +1 ou -1
        """
        if self.est_feuille():
            return self.classe
        if exemple[self.attribut] <= self.seuil:
            return self.inferieur.classifie(exemple)
        return self.superieur.classifie(exemple)
    
    def to_graph(self, g, prefixe='A'):
        """ construit une représentation de l'arbre pour pouvoir
            l'afficher
        """
        if self.est_feuille():
            g.node(prefixe,str(self.classe),shape='box')
        else:
            g.node(prefixe, str(self.attribut))
            self.inferieur.to_graph(g,prefixe+"g")
            self.superieur.to_graph(g,prefixe+"d")
            g.edge(prefixe,prefixe+"g", '<='+ str(self.seuil))
            g.edge(prefixe,prefixe+"d", '>'+ str(self.seuil))
        
        return g
    
def construit_AD(LSet,epsilon):
    """ LSet : LabeledSet
        epsilon : seuil d'entropie pour le critère d'arrêt 
    """
    
    # Si la condition sur l'entropie est déjà respectée. 
    # Alors nous arrivons au bout de l'arbre. On rend donc une feuille
    if(entropie(LSet) <= epsilon):
        un_arbre = ArbreBinaire()
        classe = classe_majoritaire(LSet)
        un_arbre.ajoute_feuille(classe)
        un_arbre.est_feuille()
        return un_arbre
    
    
    
    parameter = None # Condition sur le parametre X
    entropMin = 2    # Permet de comparer les entropies
    dim = None       # Dimension du parametre X
    
    
    input_dim = LSet.getInputDimension()
    size = LSet.size()
    
    # Récuperation de (parameter,dim)
    #print("input dimension : ",input_dim)
    for i in range(input_dim):
        #print("i : ",i)
        #print("Construit AD LSet : ",LSet.x.shape)
        entropI = discretise(LSet,i)
        
        #print("Entropie i ",entropI[1])
        if ( entropMin > entropI[1] ):
            #print("Evaluation : ", i , " // " ,entropI)
            entropMin = entropI[1]
            parameter = entropI[0]
            dim = i
    
    
    #Division des LabeledSet associés aux feuilles
    lsTab = divise(LSet,dim,parameter)
    ls1 = lsTab[0]
    ls2 = lsTab[1]
    #print("ls1 : ",ls1.size());
    #print("ls2 : ",ls2.size());
    
    
    
    #print(entropie(LSet))
    
    
    # Appel récursif pour construire les feuilles.
    un_arbre0 = construit_AD(ls1,epsilon)
    un_arbre1 = construit_AD(ls2,epsilon)
    
    un_arbre2 = ArbreBinaire()
    un_arbre2.ajoute_fils(un_arbre0,un_arbre1,dim,parameter)
    
            
    return un_arbre2

class ArbreDecision(cl.Classifier):
    # Constructeur
    def __init__(self,epsilon):
        # valeur seuil d'entropie pour arrêter la construction
        self.epsilon= epsilon
        self.racine = None
    
    # Permet de calculer la prediction sur x => renvoie un score
    def predict(self,x):
        # classification de l'exemple x avec l'arbre de décision
        # on rend 0 (classe -1) ou 1 (classe 1)
        classe = self.racine.classifie(x)
        if (classe == 1):
            return(1)
        else:
            return(-1)
    
    # Permet d'entrainer le modele sur un ensemble de données
    def train(self,set):
        # construction de l'arbre de décision 
        self.set=set
        #print(set.x.shape)
        self.racine = construit_AD(set,self.epsilon)
        

    # Permet d'afficher l'arbre
    def plot(self):
        gtree = gv.Digraph(format='png')
        return self.racine.to_graph(gtree)
        

#### TME 06 ArbreDécisionnel bagging
###
## 
def tirage(VX, m, r):
    if(r):
        tirage = []
        for i in range (m):
            choix = random.choice(VX)
            #print(choix)
            tirage.append(random.choice(VX))
    else:
        tirage = random.sample(VX,m)
    return tirage
def echantillonLS(LSet,m,r):
    input_dimension = LSet.getInputDimension()
    size = LSet.size()
    LSetRetour = ls.LabeledSet(input_dimension)
    
    ListeIndices = [i for i in range(0,size)]
    
    if(r):
        for i in range(m):
            choix = random.choice(ListeIndices)
            X = LSet.getX(choix)
            Y = LSet.getY(choix)
            LSetRetour.addExample(X,Y)
    else:
        ListeChoix = random.sample(ListeIndices,m)
        for i in ListeChoix:
            X = LSet.getX(i)
            Y = LSet.getY(i)
            LSetRetour.addExample(X,Y)
    
    return LSetRetour
class ClassifierBaggingTree(cl.Classifier):
    def __init__(self,nombre_arbre,pourcentage,seuilEntropie,remise):
        self.nombre_arbre = nombre_arbre
        self.pourcentage = pourcentage
        self.remise = remise
        self.seuilEntropie = seuilEntropie
        
    def train(self,LSet):
        #print("LSet Bagging Tree :",LSet.x.shape)
        LSetsize = LSet.size()
        nb_exemples = math.ceil(LSetsize * self.pourcentage)
        self.setLSet = []
        self.setArbre = []
        cpt = 0
        
        LSet = ut.shuffle_ls2(LSet)
        
        
        
        for i in range(self.nombre_arbre):
            LSetChoisit = echantillonLS(LSet,nb_exemples,self.remise)
            arbre = ArbreDecision(self.seuilEntropie)
            #print("CBagging : ",LSet.x.shape)
            arbre.train(LSetChoisit)
            self.setArbre.append(arbre)

        
    def predict(self,x):
        res = 0
        for i in range (len(self.setArbre)):
            res += self.setArbre[i].predict(x)
            #print(res)
        if(res >= 0):
            return 1
        else:
            return -1

def echantillonLS2(LSet,m,r):
    input_dimension = LSet.getInputDimension()
    size = LSet.size()
    LSetRetour = ls.LabeledSet(input_dimension)
    LSetExemple = ls.LabeledSet(input_dimension)
    
    ListeIndices = [i for i in range(0,size)]
    ListeChoix = []
    if(r):
        for i in range(m):
            choix = random.choice(ListeIndices)
            ListeChoix.append(choix)
            X = LSet.getX(choix)
            Y = LSet.getY(choix)
            LSetRetour.addExample(X,Y)
    else:
        ListeChoix = random.sample(ListeIndices,m)
        for i in ListeChoix:
            X = LSet.getX(i)
            Y = LSet.getY(i)
            LSetRetour.addExample(X,Y)
    
    for i in range (size):
        if i not in ListeChoix:
            LSetExemple.addExample(LSet.getX(i),LSet.getY(i))
            LSetExemple.addExample(LSet.getX(i),LSet.getY(i))
    
    return LSetRetour,LSetExemple

class ClassifierBaggingTreeOOB(ClassifierBaggingTree):
    def __init__(self,nombre_arbre,pourcentage,seuilEntropie,remise):
        self.nombre_arbre = nombre_arbre
        self.pourcentage = pourcentage
        self.remise = remise
        self.seuilEntropie = seuilEntropie
        
    def train(self,LSet):
        LSetsize = LSet.size()
        nb_exemples = math.ceil(LSetsize * self.pourcentage)
        self.setLSetTrain = []
        self.setLSetTest = []
        self.setArbre = []
        
        cpt = 0
        
        LSet = ut.shuffle_ls(LSet)
        
        
        for i in range(self.nombre_arbre):
            
            lstn,lstt = echantillonLS2(LSet,nb_exemples,self.remise)
            
            
            self.setLSetTrain.append(lstn)
            self.setLSetTest.append(lstt)
            
            arbre = ArbreDecision(self.seuilEntropie)
            arbre.train(lstn)
            self.setArbre.append(arbre)

        
    def predict(self,x):
        res = 0
        for i in range (len(self.setArbre)):
            res += self.setArbre[i].predict(x)
            #print(res)
        if(res >= 0):
            return 1
        else:
            return -1
        
    def OOB(self):
        if(self.nombre_arbre == 0):
            return 0
        acc = 0
        for i in range (self.nombre_arbre):
            acc += self.setArbre[i].accuracy(self.setLSetTest[i])
        return acc/self.nombre_arbre
    
def construit_AD_aleatoire(LSet,epsilon,nbatt):
    """ LSet : LabeledSet
        epsilon : seuil d'entropie pour le critère d'arrêt 
    """
    
    
    # Si la condition sur l'entropie est déjà respectée. 
    # Alors nous arrivons au bout de l'arbre. On rend donc une feuille
    if(entropie(LSet) <= epsilon):
        un_arbre = ArbreBinaire()
        classe = classe_majoritaire(LSet)
        un_arbre.ajoute_feuille(classe)
        un_arbre.est_feuille()
        return un_arbre
    
    parameter = None # Condition sur le parametre X
    entropMin = 2    # Permet de comparer les entropies
    dim = None       # Dimension du parametre X
    listeAttribut = [i for i in range(0,nbatt)] 
    
    input_dim = LSet.getInputDimension()
    size = LSet.size()
    
    
    
    dim = random.choice(listeAttribut)
    parameter = discretise(LSet,dim)[0]
    
    #Division des LabeledSet associés aux feuilles
    lsTab = divise(LSet,dim,parameter)
    ls1 = lsTab[0]
    ls2 = lsTab[1]
    
    
    Gain_Information = entropie(LSet) - ((entropie(ls1) + entropie(ls2))/2)
    
    if(Gain_Information <= epsilon):
        un_arbre = ArbreBinaire()
        classe = classe_majoritaire(LSet)
        un_arbre.ajoute_feuille(classe)
        un_arbre.est_feuille()
        return un_arbre
    
    # Appel récursif pour construire les feuilles.
    #print(nbatt)
    un_arbre0 = construit_AD_aleatoire(ls1,epsilon,nbatt) 
    un_arbre1 = construit_AD_aleatoire(ls2,epsilon,nbatt)
    
    un_arbre2 = ArbreBinaire()
    un_arbre2.ajoute_fils(un_arbre0,un_arbre1,dim,parameter)
    
    return un_arbre2


class ArbreDecisionAleatoire(ArbreDecision):
    def train(self,set):
        # construction de l'arbre de décision 
        self.set=set
        
    
        self.racine = construit_AD_aleatoire(set,self.epsilon,set.getInputDimension())
class ClassifierRandomForest(ClassifierBaggingTree):
    def __init__(self,nombre_arbre,pourcentage,seuilEntropie,remise):
        self.nombre_arbre = nombre_arbre
        self.pourcentage = pourcentage
        self.remise = remise
        self.seuilEntropie = seuilEntropie
        
    def train(self,LSet):
        LSetsize = LSet.size()
        nb_exemples = math.ceil(LSetsize * self.pourcentage)
        self.setLSetTrain = []
        self.setLSetTest = []
        self.setArbre = []
        
        cpt = 0
        
        LSet = ut.shuffle_ls2(LSet)
        
        
        for i in range(self.nombre_arbre):
            
            lstn,lstt = echantillonLS2(LSet,nb_exemples,self.remise)
            
            
            self.setLSetTrain.append(lstn)
            self.setLSetTest.append(lstt)
            
            arbre = ArbreDecisionAleatoire(self.seuilEntropie)
            arbre.train(lstn)
            self.setArbre.append(arbre)

        
    def predict(self,x):
        res = 0
        for i in range (len(self.setArbre)):
            res += self.setArbre[i].predict(x)
            #print(res)
        if(res >= 0):
            return 1
        else:
            return -1
        
    def OOB(self):
        if(self.nombre_arbre == 0):
            return 0
        acc = 0
        for i in range (self.nombre_arbre):
            acc += self.setArbre[i].accuracy(self.setLSetTest[i])
        return acc/self.nombre_arbre

#### TME 07 Clustering
###
## 
def normalisation(df):
    normalized_df=(df-df.min())/(df.max()-df.min())
    
    return normalized_df

def dist_euclidienne_vect(V1,V2):
    size1 = len(V1)
    size2 = len(V2)
    
    dist = 0
    if(size1 == size2):
        for i in range(size1):
            dist += (V1[i] - V2[i])**2
            
    return dist**0.5

def dist_manhattan_vect(V1,V2):
    size1 = len(V1)
    size2 = len(V2)
    
    dist = 0
    if(size1 == size2):
        for i in range(size1):
            dist += abs(V1[i] - V2[i])
            
    return dist

def dist_vect(mode,v1,v2):
    if(mode == "euclidienne"):
        return dist_euclidienne_vect(v1,v2)
    if(mode == "manhattan"):
        return dist_manhattan_vect(v1,v2)
    
def centroide(df):
    return df.mean().to_frame().T

def dist_groupes(mode,gr1,gr2):
    if(mode == "euclidienne"):
        return dist_euclidienne_vect(centroide(gr1),centroide(gr2))
    if(mode == "manhattan"):
        return dist_manhattan_vect(centroide(gr1),centroide(gr2))
    
def initialise(matrix):
    dictionnaire = dict()
    shape = matrix.shape
    for i in range (shape[0]):
        dictionnaire[i] = matrix[i,:]
    return dictionnaire

def fusionne(mode,dictionnaire):
    if(len(dictionnaire)>=2):
        
        indiceGr1 = 0
        indiceGr2 = 1
        distMin = math.inf
        
        
        for i in dictionnaire:
            for j in dictionnaire:
                if(i!=j):
                    distTemp = dist_groupes(mode,dictionnaire[i],dictionnaire[j])
                    if(distMin > distTemp):
                        indiceGr1 = i
                        indiceGr2 = j
                        distMin = distTemp
        
        newDictionnaire = dictionnaire.copy()
        
        v1 = dictionnaire[indiceGr1]
        v2 = dictionnaire[indiceGr2]
        
        newIndice = max(list(dictionnaire.keys())) + 1
        newDictionnaire[newIndice] = np.vstack( [v1,v2])
        
        del newDictionnaire[indiceGr1]
        del newDictionnaire[indiceGr2]
        
        
        print("Fusion de ",indiceGr1, " et ",indiceGr2," pour la distance min : ", distMin)
        
        return newDictionnaire,indiceGr1,indiceGr2,distMin
    else:
        return dictionnaire

def clustering_hierarchique(mode,theset):
    norm = normalisation(theset)
    M_data2DNorm= norm.as_matrix()
    # initialisation 
    courant = initialise(M_data2DNorm)       # clustering courant, au départ:s données data_2D normalisées
    M_Fusion = []                        # initialisation
    while len(courant) >=2:              # tant qu'il y a 2 groupes à fusionner
        new,k1,k2,dist_min = fusionne('euclidienne',courant)
        if(len(M_Fusion)==0):
            M_Fusion = [k1,k2,dist_min,2]
        else:
            M_Fusion = np.vstack( [M_Fusion,[k1,k2,dist_min,2] ])
        courant = new


    # Paramètre de la fenêtre d'affichage: 
    plt.figure(figsize=(30, 15)) # taille : largeur x hauteur
    plt.title('Dendrogramme', fontsize=25)    
    plt.xlabel('Exemple', fontsize=25)
    plt.ylabel('Distance', fontsize=25)

    # Construction du dendrogramme à partir de la matrice M_Fusion:
    scipy.cluster.hierarchy.dendrogram(
        M_Fusion,
        leaf_font_size=18.,  # taille des caractères de l'axe des X
    )

    # Affichage du résultat obtenu:
    plt.show()
    
def dist_max_groupes(mode,gr1,gr2):
    distMax = -math.inf
    
    shape1 = gr1.shape
    shape2 = gr2.shape    
    
    
    for i in gr1:
        for j in gr2:
            if(len(shape1) == 1):
                v1 = gr1
            else:
                v1 = i
            if(len(shape2) == 1):
                v2 = gr2
            else:
                v2 = j
            distTemp = dist_vect(mode,v1,v2) 
            if (distTemp > distMax):
                distMax = distTemp
    return distMax

def fusionneMax(mode,dictionnaire):
    if(len(dictionnaire)>=2):
        
        indiceGr1 = 0
        indiceGr2 = 1
        distMin = math.inf
        
        
        for i in dictionnaire:
            for j in dictionnaire:
                if(i!=j):
                    distTemp = dist_max_groupes(mode,dictionnaire[i],dictionnaire[j])
                    if(distMin > distTemp):
                        indiceGr1 = i
                        indiceGr2 = j
                        distMin = distTemp
        
        newDictionnaire = dictionnaire.copy()
        
        v1 = dictionnaire[indiceGr1]
        v2 = dictionnaire[indiceGr2]
        
        
        newIndice = max(list(dictionnaire.keys())) + 1
        newDictionnaire[newIndice] = np.vstack( [v1,v2])
        
        del newDictionnaire[indiceGr1]
        del newDictionnaire[indiceGr2]
        
        
        print("Fusion de ",indiceGr1, " et ",indiceGr2," pour la distance min : ", distMin)
        
        return newDictionnaire,indiceGr1,indiceGr2,distMin
    else:
        return dictionnaire
    
def clustering_hierarchique_distMax(mode,theset):
    norm = normalisation(theset)
    M_data2DNorm= norm.as_matrix()
    # initialisation 
    courant = initialise(M_data2DNorm)       # clustering courant, au départ:s données data_2D normalisées
    M_Fusion = []                        # initialisation
    while len(courant) >=2:              # tant qu'il y a 2 groupes à fusionner
        new,k1,k2,dist_min = fusionneMax(mode,courant)
        if(len(M_Fusion)==0):
            M_Fusion = [k1,k2,dist_min,2]
        else:
            M_Fusion = np.vstack( [M_Fusion,[k1,k2,dist_min,2] ])
        courant = new


    # Paramètre de la fenêtre d'affichage: 
    plt.figure(figsize=(30, 15)) # taille : largeur x hauteur
    plt.title('Dendrogramme', fontsize=25)    
    
def clustering_hierarchique_distMax(mode,theset):
    norm = normalisation(theset)
    M_data2DNorm= norm.as_matrix()
    # initialisation 
    courant = initialise(M_data2DNorm)       # clustering courant, au départ:s données data_2D normalisées
    M_Fusion = []                        # initialisation
    while len(courant) >=2:              # tant qu'il y a 2 groupes à fusionner
        new,k1,k2,dist_min = fusionneMax(mode,courant)
        if(len(M_Fusion)==0):
            M_Fusion = [k1,k2,dist_min,2]
        else:
            M_Fusion = np.vstack( [M_Fusion,[k1,k2,dist_min,2] ])
        courant = new


    # Paramètre de la fenêtre d'affichage: 
    plt.figure(figsize=(30, 15)) # taille : largeur x hauteur
    plt.title('Dendrogramme', fontsize=25)    
    plt.xlabel('Exemple', fontsize=25)
    plt.ylabel('Distance', fontsize=25)

    # Construction du dendrogramme à partir de la matrice M_Fusion:
    scipy.cluster.hierarchy.dendrogram(
        M_Fusion,
        leaf_font_size=18.,  # taille des caractères de l'axe des X
    )

    # Affichage du résultat obtenu:
    plt.show()
        

#### TME 08
###
## 
def inertie_cluster(df):
    cf = centroide(df)
    
    shape = df.shape
    inertie = 0
    

    
    for i in range(shape[0]):
        inertie += dist_vect("euclidienne",df.iloc[i],cf.iloc[0])**2
    
    return inertie


def removeList(liste,listeR):
    retour = liste
    for i in listeR:
        retour.remove(i)
    return retour

def initialisation(n,df):
    shape = df.shape
    listeElement = list(range(shape[0]))
    
    nbTirage = math.ceil(shape[0]/n)
    
    listeDF = []
    
    for i in range(n):
        listeEnsemble = tirage(listeElement,1,False)
        listeElement = removeList(listeElement,listeEnsemble)
        
        newDF = df.iloc[listeEnsemble[0]].to_frame().T
        
        listeDF.append(newDF)
        
    
    result = pd.concat(listeDF)
    
    return result

def plus_proche(exemple,df):
    size = df.shape[0]
    dist_min = math.inf
    indice_min = -1
    for i in range(size):
        dist = dist_vect("euclidienne",df.iloc[i],exemple)
        if(dist < dist_min):
            indice_min = i
            dist_min = dist
     
    return indice_min

def affecte_cluster(df,ensemble):
    dictionnaire = dict()
    for i in range(ensemble.shape[0]):
        dictionnaire[i] = []
    
    for i in range(df.shape[0]):
        indice = plus_proche(df.iloc[i],ensemble)
        

        dictionnaire[indice].append(i)
    
    return dictionnaire

def nouveaux_centroides(df,mAffect):    
    listeDF = []
    for l in mAffect.values():
        liste = []
        for i in l:
            liste.append(i)
        newDF = centroide(df.loc[liste])
        listeDF.append(newDF)
    result = pd.concat(listeDF)
    return result

def inertie_globale(df,mAffect):
    
    inertieGlobale = 0
    
    
    for l in mAffect.values():    
        liste = []
        
        for i in l:
            liste.append(i)
        
        
        
        
        newDF = df.loc[liste]
        inertieCluster = inertie_cluster(newDF)
        inertieGlobale += inertieCluster
        
        
    return inertieGlobale

def kmoyennes(k,df,epsilon,iter_max):
    centroides = initialisation(k,df)
    mAffect = affecte_cluster(df,centroides)
    inertie = math.inf
    newInertie = inertie_globale(df,mAffect)
    
    
    
    print("initialisation",mAffect)
    
    for i in range(iter_max):
        difference = abs(newInertie - inertie) 
        
        if(inertie == math.inf):
            difference = inertie
            
        if(difference < epsilon and difference != math.inf):
            break;
        
        
        
        centroides = nouveaux_centroides(df,mAffect)
        mAffect = affecte_cluster(df,centroides)
        
        
        inertie = newInertie
        newInertie = inertie_globale(df,mAffect)
        print("Itération ",i," mAffect : ",mAffect)
        
        
    return centroides,mAffect

def affiche_resultat(df,centroides,mAffect):
    listeColor =['b', 'g', 'c', 'm', 'y', 'k', 'w','r']
    
    
    cpt = 0
    for l in mAffect.values():    
        liste = []
        
        for i in l:
            liste.append(i)
        
        newDF = df.loc[liste]
        
        plt.scatter(newDF['X'],newDF['Y'],color=listeColor[cpt])
        cpt += 1
    
    plt.scatter(les_centres['X'],les_centres['Y'],color='r',marker='x')

#### TME 09
###
## 
def dist_intracluster(ensembleExemple):    

    max_dist = -1
    
    #print(ensembleExemple)
    for i in ensembleExemple.index:
        
        exemple1 = ensembleExemple.loc[i]
        for j in ensembleExemple.index:
            exemple2 = ensembleExemple.loc[j]

            dist = km.dist_euclidienne_vect(exemple1,exemple2)
            if(dist > max_dist):
                max_dist = dist
                indice_i = i
                indice_j = j
                
    #print(indice_i,indice_j)
    #print(ensembleExemple.iloc[indice_i],ensembleExemple.iloc[indice_j])
    return(max_dist)

            
def global_intraclusters(df,mAffect):
    max_dist = -1
    for k in range(0,len(mAffect)):
        DF = DataRandom.iloc[mAffect[k]]
        dist = dist_intracluster(DF)

        if(dist > max_dist):
            max_dist = dist
    return max_dist

def sep_clusters(centroides):
    dist_min = math.inf
    for index1,row1 in centroides.iterrows():
        for index2,row2 in centroides.iterrows():
            dist = km.dist_euclidienne_vect(row1,row2)
            
            if(dist < dist_min and not row1.equals(row2)):
                dist_min = dist
    return dist_min

def evaluation(mode,df,centroides,mAffect):
    if(mode == "Dunn"):
        return global_intraclusters(df,mAffect) / sep_clusters(les_centres)
    if(mode == "XB"):
        return km.inertie_globale(df,mAffect) / sep_clusters(les_centres)
    
    
def bestClusters(DF,epsilon,iter_max):
    
    indiceDunn = []
    indiceXB = []
    
    for i in range(2,11):
        les_centres, l_affectation = km.kmoyennes(i, DF, 0.05, 100)  
        evaluationDunn = evaluation("Dunn",DataRandom,les_centres,l_affectation)
        evaluationXB = evaluation("XB",DataRandom,les_centres,l_affectation)
        
        indiceDunn.append(evaluationDunn)
        indiceXB.append(evaluationXB)
        
        print("- - - - - - - - -")
        print("Nombre cluster : ",i)
        print("\tDunn:\t"+str(evaluationDunn))
        print("\tXB:\t"+str(evaluationXB))
        print("- - - - - - - - -")
        
    return(indiceDunn,indiceXB)
