# IA & Data Analysis in French

## Description

This is a recap of the skills I acquired following a course about IA and Data Analysis.

## Prerequisites 

- Jupyter-Notebook 
- Python 3.0+

## Running 

You can either open the jupyter-notebook within the Gitlab GUI but you will not be able to edit and run cells.

Otherwise you download the file .ipynb and opens in within Jupyter-Notebook.

## Authors 
- Jérémy Nguyen
- Sorbonne Université Sciences 